
window.onload = function(){
    console.log("jquery load");
    $( document ).tooltip();

    function progress(){
      var iniprogress = $('.bar').css('width');
      var partes = 190/5;
      var $statusprogress = parseInt(iniprogress);
      var newstatus = $statusprogress+partes;
      $('.bar').css('width', ''+newstatus+'px')
      console.log(newstatus);
    }
    var i;
    var type=0
    for (i = 1; i < 10; i++) {
      type++
      if(type>3){type = 1}
        $('#contDrag').append('<div class="itemDrag" data-type="'+type+'" data-drag="'+i+'"><img src="img/item-'+ i +'.png" alt=""></div>');
    }
    var dragValor;
    var dropValor;
    var correcto=0;
    var $dragableitem = $('.itemDrag');
    var $dropableitem = $(".trash");
    var countdrop = 9;
    var newcountdrop = 0;

    $dragableitem.draggable({
        revert: true,
        scroll: true,
        scrollSpeed:100,
        scrollSensitivity:10,
        start: function() {
            dragValor = $(this).data('type');
            console.log(dragValor);
            $dropableitem.removeClass("animated shake bounce");
        }
    })
    $dropableitem.droppable({
          tolerance: "intersect",
          over: function(){$(this).addClass('light-drop')},
          out:function(){$(this).removeClass('light-drop')},
          drop: function(event, ui) {
            setTimeout(function () {
                $dropableitem.removeClass("animated shake bounce");
            }, 1500);
            $(this).removeClass('light-drop')
            dropValor = $(this).attr('data-trash');
            newcountdrop++
            if(newcountdrop == countdrop){
                $("#caja").css("display",'block').addClass('animated bounceInDown');
                $(".modaltrue").css('background-image' , 'url(img/finish-back-rept.png)');
                $('.button-continuar').css('background-image' , 'url(img/rep-btn.png)')
                $('.number-incorrect').html(9-correcto);
            }
            console.log(newcountdrop);
            if (dragValor == dropValor) {
              console.log("Correcto");
              $(this).addClass("animated bounce");
              progress();
              ui.draggable.remove();
              correcto++;
              $(".score h2").html(correcto);
              if(correcto==5){
                $('.score').addClass('score-finish animated bounce')
                setTimeout(function () {
                  $("#caja").css("display",'block').addClass('animated bounceInDown');
                }, 1000);
              }
            }else{
              ui.draggable.addClass('no-trash');
              console.log("Incorrecto");
              $(this).addClass("animated shake");
            }
          }
        });
}
