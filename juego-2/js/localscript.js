
window.onload = function(){

    console.log("jquery load");
    $( document ).tooltip();

    function progress(){
      var iniprogress = $('.bar').css('width');
      var partes = 190/6;
      var $statusprogress = parseInt(iniprogress);
      var newstatus = $statusprogress+partes;
      $('.bar').css('width', ''+newstatus+'px')
      console.log(newstatus);
    }

    var $correcto = 0;
    var conuntclick = 0;
    var nameitem;
    var itemuno;
    var itemdos;
    var $div
    var divuno;
    var divdos;
    $('.item').on('click', function(){
      var $ahora =$(this);
      conuntclick++
      nameitem = $ahora.data("name");
      $div = $ahora.data("item");
      console.log(nameitem);
      console.log("el div",  $div)
      $ahora.css('transform', 'rotateY(180deg)');
      setTimeout(function () {
        $('img' ,$ahora).css('display', 'block');
      }, 200);
      $ahora.addClass('select');
      if (conuntclick == 1){
        itemuno = nameitem;
        divuno = $div;
      }
      else{
        itemdos = nameitem;
        divdos = $div;
        conuntclick = 0;

        var divunocompleto =$('.item[data-item='+divuno+']')
        var divdoscompleto =$('.item[data-item='+divdos+']')
        setTimeout(function () {
          if(itemuno == itemdos){
            divunocompleto.removeClass('select').addClass('correcto animated rubberBand');
            divdoscompleto.removeClass('select').addClass('correcto animated rubberBand');
            progress();
            $correcto++;
            $(".score h2").html($correcto);
            if($correcto==6){
              $('.score').addClass('score-finish animated bounce')
              setTimeout(function () {
                $("#caja").css("display",'block').addClass('animated bounceInDown');
                var altoVentana=$('.contGame').height();
                var anchoVentana=$('.contGame').width();
                var scrollVentana=$(document).scrollTop();
                var alto=(altoVentana-$(".modaltrue").height())/2;
                var ancho=(anchoVentana-$(".modaltrue").width())/2;
                $("#caja").css("top",scrollVentana);
                $(".modaltrue").css("top",alto)
                $(".modaltrue").css("left",ancho);
              }, 1000);

            }

          }else{
            $('img' ,$ahora).css('display', 'none');
            $ahora.css('transform', 'rotateY(0deg)');
            divunocompleto.removeClass('select').css('transform', 'rotateY(0deg)');
            divdoscompleto.removeClass('select').css('transform', 'rotateY(0deg)');
            $('img' , divunocompleto).css('display', 'none');
            $('img' , divdoscompleto).css('display', 'none');

          }
        }, 600);
      }
    })
}
