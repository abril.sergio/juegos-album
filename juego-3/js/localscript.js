
window.onload = function(){
    console.log("jquery load");
    $( document ).tooltip();

    function progress(){
      var iniprogress = $('.bar').css('width');
      var partes = 190/1;
      var $statusprogress = parseInt(iniprogress);
      var newstatus = $statusprogress+partes;
      $('.bar').css('width', ''+newstatus+'px')
      console.log(newstatus);
    }

    var i;
    var type=0
    var dragValor;
    var dropValor;
    var correcto=0;
    var incorrecto=0;
    var evalua=0;
    var $dragableitem = $('.word-drag');
    var $dropableitem = $(".name-fruit");

    $dragableitem.draggable({
        revert: true,
        scroll: true,
        scrollSpeed:100,
        scrollSensitivity:10,
        start: function() {
            dragValor = $(this).data('drag');
            console.log(dragValor);
            $dropableitem.removeClass("animated shake bounce");
        }
    })
    $dropableitem.droppable({
          tolerance: "intersect",
          over: function(){$(this).addClass('light-drop')},
          out:function(){$(this).removeClass('light-drop')},
          drop: function(event, ui) {
            $(this).removeClass('light-drop')
            dropValor = $(this).attr('data-namefruit');
            ui.draggable.remove();
            $(this).html(dragValor)
            evalua++;
            var $drop = $(this).html();
            var $fruit = $(this).data('namefruit');
            if($drop == $fruit){
              correcto++;
              console.log('correcto', correcto)
            }else{
              incorrecto++;
              console.log('incorrecto', incorrecto)
            }
            function seemodal(){
              $("#caja").css("display",'block').addClass('animated bounceInDown');
            }
            function seemodalincorrect(){
              $("#caja").css("display",'block').addClass('animated bounceInDown');
              $(".modaltrue").css('background-image' , 'url(img/finish-back-rept.png)');
              $('.button-continuar').css('background-image' , 'url(img/rep-btn.png)')
              $('.number-incorrect').html(incorrecto);
            }
            if(evalua==5){
                var $namefruit = []
                var $htmldrop = []
                $dropableitem.each(function(){
                  $htmldrop.push($(this).html());
                  $namefruit.push($(this).data('namefruit'))
                })
                console.log($htmldrop , $namefruit);
                if($htmldrop[0] == $namefruit[0] & 
                  $htmldrop[1] == $namefruit[1] &
                  $htmldrop[2] == $namefruit[2] &
                  $htmldrop[3] == $namefruit[3] &
                  $htmldrop[4] == $namefruit[4]){
                  $('.score').addClass('score-finish animated bounce')
                  progress();
                  setTimeout(function () {
                  seemodal();
                  $(".score h2").html(correcto);
                    
                  }, 1000);
                  console.log('correcto')
                  }else{
                    seemodalincorrect();
                    console.log('incorrecto');
                  }
            }
          }
        });
}
