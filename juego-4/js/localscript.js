(function(d){d.fn.shuffle=function(c){c=[];return this.each(function(){c.push(d(this).clone(true))}).each(function(a,b){d(b).replaceWith(c[a=Math.floor(Math.random()*c.length)]);c.splice(a,1)})};d.shuffle=function(a){return d(a).shuffle()}})(jQuery);

window.onload = function(){
    console.log("jquery load");
    $( document ).tooltip();


    var frase = 'La naturaleza es vida y paz'
    var fraseseparada = frase.split(' ')
    var count = fraseseparada.length
    // console.log(frase)
    // console.log(fraseseparada)
    // console.log(count)
 
    var paso;
    var j
    for (paso = 0; paso < count; paso++) {
      $('.cont-fruits').append('<div class="name-fruit" data-namefruit="'+fraseseparada[paso]+'"></div>');
      $('.cont-words').append('<div class="word-drag" data-drag="'+fraseseparada[paso]+'"><h2>'+ fraseseparada[paso]+'</h2> </div>')
    };
    $('.word-drag').shuffle();
    jQuery.fn.shuffle = function () {
    var j;
    for (var i = 0; i < this.length; i++) {
        j = Math.floor(Math.random() * this.length);
        $(this[i]).before($(this[j]));
    }
    return this;
    };

    function progress(){
      var iniprogress = $('.bar').css('width');
      var partes = 190/1;
      var $statusprogress = parseInt(iniprogress);
      var newstatus = $statusprogress+partes;
      $('.bar').css('width', ''+newstatus+'px')
      console.log(newstatus);
    }

    var i;
    var type=0
    var dragValor;
    var dropValor;
    var correcto=0;
    var incorrecto=0;
    var evalua=0;
    var $dragableitem = $('.word-drag');
    var $dropableitem = $(".name-fruit");

    $dragableitem.draggable({
        revert: true,
        scroll: true,
        scrollSpeed:100,
        scrollSensitivity:10,
        start: function() {
            dragValor = $(this).data('drag');
            console.log(dragValor);
            $dropableitem.removeClass("animated shake bounce");
        }
    })
    $dropableitem.droppable({
          tolerance: "intersect",
          over: function(){$(this).addClass('light-drop')},
          out:function(){$(this).removeClass('light-drop')},
          drop: function(event, ui) {
            $(this).removeClass('light-drop')
            dropValor = $(this).attr('data-namefruit');
            ui.draggable.remove();
            $(this).html(dragValor)
            evalua++;
            var $drop = $(this).html();
            var $fruit = $(this).data('namefruit');
            if($drop == $fruit){
              correcto++;
              console.log('correcto', correcto)
            }else{
              incorrecto++;
              console.log('incorrecto', incorrecto)
            }
            function seemodal(){
              $("#caja").css("display",'block').addClass('animated bounceInDown');
            }
            function seemodalincorrect(){
              $("#caja").css("display",'block').addClass('animated bounceInDown');
              $(".modaltrue").css('background-image' , 'url(img/finish-back-rept.png)');
              $('.button-continuar').css('background-image' , 'url(img/rep-btn.png)')
              $('.number-incorrect').html(incorrecto);
            }
            if(evalua==count){
                var $namefruit = []
                var $htmldrop = []
                $dropableitem.each(function(){
                  $htmldrop.push($(this).html());
                  $namefruit.push($(this).data('namefruit'))
                })
                console.log($htmldrop , $namefruit);
                if($htmldrop[0] == $namefruit[0] & 
                  $htmldrop[1] == $namefruit[1] &
                  $htmldrop[2] == $namefruit[2] &
                  $htmldrop[3] == $namefruit[3] &
                  $htmldrop[4] == $namefruit[4]){
                  $('.score').addClass('score-finish animated bounce')
                  progress();
                  setTimeout(function () {
                  seemodal();
                  $(".score h2").html(correcto);
                    
                  }, 1000);
                  console.log('correcto')
                  }else{
                    seemodalincorrect();
                    console.log('incorrecto');
                  }
            }
          }
        });
}
